public class Board
{
	private Die die1;
	private Die die2;
	private boolean[] tiles;
	
	public Board()
	{
		this.die1 = new Die();
		this.die2 = new Die();
		this.tiles = new boolean[12];
	}
	public boolean playATurn()
	{
		this.die1.roll();
		this.die2.roll();
		System.out.println(this.die1);
		System.out.println(this.die2);
		int sumOfDice = this.die1.getFaceValue() + this.die2.getFaceValue();
		
		if(!this.tiles[sumOfDice -1])
		{
			this.tiles[sumOfDice -1] = true;
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			return false;
		}
		else if(!this.tiles[this.die1.getFaceValue() -1])
		{
			this.tiles[this.die1.getFaceValue()-1] = true;
			System.out.println("Closing tile with same value as die one: " + this.die1.getFaceValue());
			return false;
		}
		else if(!this.tiles[this.die2.getFaceValue() -1])
		{
			this.tiles[this.die2.getFaceValue()-1] = true;
			System.out.println("Closing tile with same value as die two: " + this.die2.getFaceValue());
			return false;
		}
		else
		{
			System.out.println("All the tiles for these values are already shut");
			return true;
		}
			
	}
	public String toString()
	{
		String rolled = "";
		for(int i=0; i<this.tiles.length;i++)
		{
			if(this.tiles[i])
			{
				rolled += "X"; 
			}
			else
			{
				rolled += i+1;
			}
			rolled += " ";
		}
		return rolled;
	}
}