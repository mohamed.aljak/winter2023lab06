public class Jackpot
{
	public static void main(String[] args)
	{
		System.out.println("Welcome Player!");
		Board board = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		while(!gameOver)
		{
			System.out.println(board);
			if(board.playATurn())
			{
				gameOver = true;
			}
			else
			{
				numOfTilesClosed ++;
			}
			
		}
		if(numOfTilesClosed >= 7)
		{
			System.out.println("Jackpot!");
		}
		else
		{
			System.out.println("Game Over!");
		}
	}
}